'use strict';

//const urlParse = require('url-parse');

String.prototype.replaceAll = function (search, replacement) {
  const target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

document.onreadystatechange = () => {
  // Create elements needed wrapping the form

  const outputElem = document.querySelector(
    '#form_wrapper-generator',
  );

  const sourceFormUrl = 'http://volvo-iframe-form.test';

  /**
   * Fetch URL
   * @param  {String}   sourceFormUrl      The URL to get HTML from
   */

  fetch(sourceFormUrl)

    .then(function (response) {
      // When the page is loaded convert it to text
      return response.text()
    })
    .then(function (html) {
      // Initialize the DOM parser
      const parser = new DOMParser();

      // Parse the text
      const response = parser.parseFromString(html, "text/html");

      // Checking if url is absolute or relative
      const isAbsoluteUrl = urlString =>
        urlString.indexOf('http://') === 0 ||
        urlString.indexOf('https://') === 0;

      const scriptNodes = response.querySelectorAll('script');
      const formElem = response.querySelector('.container');

      // iterate over all scripts
      for (let i = 0; i < scriptNodes.length; i++) {
        const scriptNode = scriptNodes[i];
        // create script element
        const scriptElem = document.createElement('script');

        // iterate over all script attributes
        for (let i = 0; i < scriptNode.attributes.length; i++) {
          const attr = scriptNode.attributes[i];
          scriptElem.setAttribute(attr.name, attr.value);

          // rewrite relative to absolute paths
          scriptElem.src = isAbsoluteUrl(
            scriptNode.getAttribute('src'),
          )
            ? scriptNode.getAttribute('src')
            : sourceFormUrl + '/' + scriptNode.getAttribute('src');
        }

        // create scripts elems if don't exist
        if (
          document.querySelector(
            'script[src="' + scriptElem.src + '"]',
          ) == null
        ) {
          document.body.appendChild(scriptElem);
        }
      }
      const styleNodes = response.querySelectorAll(
        'link[rel="stylesheet"]',
      );

      for (let i = 0; i < styleNodes.length; i++) {
        const styleNode = styleNodes[i];
        const styleHref = isAbsoluteUrl(
          styleNode.getAttribute('href'),
        )
          ? styleNode.getAttribute('href')
          : sourceFormUrl + '/' + styleNode.getAttribute('href');
        const styleElem = document.createElement('link');

        styleElem.rel = 'stylesheet';
        styleElem.type = 'text/css';
        styleElem.href = styleHref;

        // create style elems if don't exist
        if (
          document.querySelector('link[href="' + styleHref + '"]') ==
          null
        ) {
          document
            .getElementsByTagName('head')[0]
            .appendChild(styleElem);
        }
      }


      const imgNodes = response.querySelectorAll('img[src]');
      // iterate over all imgs
      for (let i = 0; i < imgNodes.length; i++) {
        const imgNode = imgNodes[i];
        if (
          !isAbsoluteUrl(imgNode.getAttribute('src')) &&
          !imgNode.getAttribute('src').includes('../')
        ) {
          imgNode.setAttribute(
            'src',
            sourceFormUrl + '/' + imgNode.getAttribute('src'),
          );
        } else if (imgNode.getAttribute('src').includes('../')) {
          const imgRewrittenRelSrc = imgNode
            .getAttribute('src')
            .replace('../', '');
          imgNode.setAttribute(
            'src',
            sourceFormUrl + '/' + imgRewrittenRelSrc,
          );
        }
      }

      outputElem.innerHTML = formElem.innerHTML;
    })
    .catch(function (err) {
      console.log('Failed to fetch page: ', err);
    })
  }